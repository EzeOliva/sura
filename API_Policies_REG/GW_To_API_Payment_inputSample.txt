{
  "paymentPlans": [
    {
      "publicID": "pp:02",
      "name": "Pago Total - ARS",
      "currency": "ars",
      "reporting": false,
      "policyLevelBillingBillDateOrDueDateBilling": "BillDateBilling",
      "periodicity": "monthly",
      "maximumNumberOfInstallments": 0
    },
    {
      "publicID": "pp:04",
      "name": "2 Cuotas - ARS",
      "currency": "ars",
      "reporting": false,
      "policyLevelBillingBillDateOrDueDateBilling": "BillDateBilling",
      "periodicity": "monthly",
      "maximumNumberOfInstallments": 2
    },
    {
      "publicID": "pp:06",
      "name": "3 Cuotas - ARS",
      "currency": "ars",
      "reporting": false,
      "policyLevelBillingBillDateOrDueDateBilling": "BillDateBilling",
      "periodicity": "monthly",
      "maximumNumberOfInstallments": 3
    }
  ]
}